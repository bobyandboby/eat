PGDMP     -    /                w            eat    10.4    10.4 ,    �	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �	           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �	           1262    41198    eat    DATABASE     u   CREATE DATABASE eat WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE eat;
             postgres    false                        2615    41199    eat    SCHEMA        CREATE SCHEMA eat;
    DROP SCHEMA eat;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             romainvaidie    false            �	           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  romainvaidie    false    3                        3079    12544    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �	           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    41200    actor    TABLE       CREATE TABLE eat.actor (
    mail character varying(100) NOT NULL,
    "firstName" character varying(50) NOT NULL,
    "lastName" character varying(50) NOT NULL,
    "phoneNumber" character varying(20) NOT NULL,
    "adressCP" character varying(5),
    "adressCity" character varying(100) NOT NULL,
    "adressStreet" character varying(100),
    "adressCountry" character varying(100) NOT NULL,
    "codeType" character varying(10) NOT NULL,
    "idOrganization" character varying(20),
    "dateBirthday" date
);
    DROP TABLE eat.actor;
       eat         postgres    false    5            �            1259    41228    category    TABLE     p   CREATE TABLE eat.category (
    code character varying(5) NOT NULL,
    label character varying(50) NOT NULL
);
    DROP TABLE eat.category;
       eat         postgres    false    5            �            1259    41213    establishment    TABLE     f  CREATE TABLE eat.establishment (
    "adressStreet" character varying(50) NOT NULL,
    "adressCountry" character varying(50) NOT NULL,
    "adressCity" character varying(50) NOT NULL,
    "adressCP" character varying(5),
    "phoneNumber" character varying(10),
    name character varying(50) NOT NULL,
    "organizationId" character varying(5) NOT NULL
);
    DROP TABLE eat.establishment;
       eat         postgres    false    5            �            1259    41243    order    TABLE     �   CREATE TABLE eat."order" (
    id character varying(9) NOT NULL,
    date date NOT NULL,
    "mailActor" character varying(100) NOT NULL,
    "idProduct" character varying(50) NOT NULL
);
    DROP TABLE eat."order";
       eat         postgres    false    5            �            1259    41218    organization    TABLE     `  CREATE TABLE eat.organization (
    id character varying(5) NOT NULL,
    name character varying(50) NOT NULL,
    "phoneNumber" character varying(10) NOT NULL,
    "adressStreet" character varying(50) NOT NULL,
    "adressCity" character varying(50) NOT NULL,
    "adressCountry" character varying(50) NOT NULL,
    "adressCP" character varying(5)
);
    DROP TABLE eat.organization;
       eat         postgres    false    5            �            1259    41223    product    TABLE     k  CREATE TABLE eat.product (
    "productionDate" date NOT NULL,
    price double precision,
    name character varying(100),
    "dateForSale" date,
    "dateConsumerDeadline" date,
    quantity bigint,
    "codeUnity" character varying(5),
    "codeCategory" character varying(5),
    "mailActor" character varying(100),
    id character varying(100) NOT NULL
);
    DROP TABLE eat.product;
       eat         postgres    false    5            �            1259    41238    status    TABLE     n   CREATE TABLE eat.status (
    code character varying(5) NOT NULL,
    label character varying(50) NOT NULL
);
    DROP TABLE eat.status;
       eat         postgres    false    5            �            1259    41208 	   typeActor    TABLE     t   CREATE TABLE eat."typeActor" (
    code character varying(10) NOT NULL,
    label character varying(50) NOT NULL
);
    DROP TABLE eat."typeActor";
       eat         postgres    false    5            �            1259    41233    unity    TABLE     m   CREATE TABLE eat.unity (
    code character varying(5) NOT NULL,
    label character varying(50) NOT NULL
);
    DROP TABLE eat.unity;
       eat         postgres    false    5            �	          0    41200    actor 
   TABLE DATA               �   COPY eat.actor (mail, "firstName", "lastName", "phoneNumber", "adressCP", "adressCity", "adressStreet", "adressCountry", "codeType", "idOrganization", "dateBirthday") FROM stdin;
    eat       postgres    false    197   �1       �	          0    41228    category 
   TABLE DATA               ,   COPY eat.category (code, label) FROM stdin;
    eat       postgres    false    202   �2       �	          0    41213    establishment 
   TABLE DATA               �   COPY eat.establishment ("adressStreet", "adressCountry", "adressCity", "adressCP", "phoneNumber", name, "organizationId") FROM stdin;
    eat       postgres    false    199   �2       �	          0    41243    order 
   TABLE DATA               B   COPY eat."order" (id, date, "mailActor", "idProduct") FROM stdin;
    eat       postgres    false    205   ,3       �	          0    41218    organization 
   TABLE DATA               w   COPY eat.organization (id, name, "phoneNumber", "adressStreet", "adressCity", "adressCountry", "adressCP") FROM stdin;
    eat       postgres    false    200   I3       �	          0    41223    product 
   TABLE DATA               �   COPY eat.product ("productionDate", price, name, "dateForSale", "dateConsumerDeadline", quantity, "codeUnity", "codeCategory", "mailActor", id) FROM stdin;
    eat       postgres    false    201   �3       �	          0    41238    status 
   TABLE DATA               *   COPY eat.status (code, label) FROM stdin;
    eat       postgres    false    204   �3       �	          0    41208 	   typeActor 
   TABLE DATA               /   COPY eat."typeActor" (code, label) FROM stdin;
    eat       postgres    false    198   4       �	          0    41233    unity 
   TABLE DATA               )   COPY eat.unity (code, label) FROM stdin;
    eat       postgres    false    203   94       	           2606    41232    category Category_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY eat.category
    ADD CONSTRAINT "Category_pkey" PRIMARY KEY (code);
 ?   ALTER TABLE ONLY eat.category DROP CONSTRAINT "Category_pkey";
       eat         postgres    false    202            	           2606    41207    actor actor_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY eat.actor
    ADD CONSTRAINT actor_pkey PRIMARY KEY (mail);
 7   ALTER TABLE ONLY eat.actor DROP CONSTRAINT actor_pkey;
       eat         postgres    false    197            	           2606    41217     establishment establishment_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY eat.establishment
    ADD CONSTRAINT establishment_pkey PRIMARY KEY ("adressStreet", "adressCountry", "adressCity");
 G   ALTER TABLE ONLY eat.establishment DROP CONSTRAINT establishment_pkey;
       eat         postgres    false    199    199    199            "	           2606    41296    order order_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY eat."order"
    ADD CONSTRAINT order_pkey PRIMARY KEY ("idProduct", id, "mailActor");
 9   ALTER TABLE ONLY eat."order" DROP CONSTRAINT order_pkey;
       eat         postgres    false    205    205    205            	           2606    41222    organization organization_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY eat.organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id);
 E   ALTER TABLE ONLY eat.organization DROP CONSTRAINT organization_pkey;
       eat         postgres    false    200            	           2606    41289    product product_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY eat.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);
 ;   ALTER TABLE ONLY eat.product DROP CONSTRAINT product_pkey;
       eat         postgres    false    201             	           2606    41242    status status_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY eat.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (code);
 9   ALTER TABLE ONLY eat.status DROP CONSTRAINT status_pkey;
       eat         postgres    false    204            	           2606    41212    typeActor type_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY eat."typeActor"
    ADD CONSTRAINT type_pkey PRIMARY KEY (code);
 <   ALTER TABLE ONLY eat."typeActor" DROP CONSTRAINT type_pkey;
       eat         postgres    false    198            	           2606    41237    unity unity_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY eat.unity
    ADD CONSTRAINT unity_pkey PRIMARY KEY (code);
 7   ALTER TABLE ONLY eat.unity DROP CONSTRAINT unity_pkey;
       eat         postgres    false    203            '	           2606    41263    product codeCategory    FK CONSTRAINT     {   ALTER TABLE ONLY eat.product
    ADD CONSTRAINT "codeCategory" FOREIGN KEY ("codeCategory") REFERENCES eat.category(code);
 =   ALTER TABLE ONLY eat.product DROP CONSTRAINT "codeCategory";
       eat       postgres    false    2332    202    201            #	           2606    41248    actor codeType    FK CONSTRAINT     t   ALTER TABLE ONLY eat.actor
    ADD CONSTRAINT "codeType" FOREIGN KEY ("codeType") REFERENCES eat."typeActor"(code);
 7   ALTER TABLE ONLY eat.actor DROP CONSTRAINT "codeType";
       eat       postgres    false    2324    198    197            &	           2606    41258    product codeUnity    FK CONSTRAINT     r   ALTER TABLE ONLY eat.product
    ADD CONSTRAINT "codeUnity" FOREIGN KEY ("codeUnity") REFERENCES eat.unity(code);
 :   ALTER TABLE ONLY eat.product DROP CONSTRAINT "codeUnity";
       eat       postgres    false    203    2334    201            $	           2606    41253    actor idOrganization    FK CONSTRAINT        ALTER TABLE ONLY eat.actor
    ADD CONSTRAINT "idOrganization" FOREIGN KEY ("idOrganization") REFERENCES eat.organization(id);
 =   ALTER TABLE ONLY eat.actor DROP CONSTRAINT "idOrganization";
       eat       postgres    false    200    2328    197            *	           2606    41297    order idProduct    FK CONSTRAINT     i   ALTER TABLE ONLY eat."order"
    ADD CONSTRAINT "idProduct" FOREIGN KEY (id) REFERENCES eat.product(id);
 :   ALTER TABLE ONLY eat."order" DROP CONSTRAINT "idProduct";
       eat       postgres    false    205    2330    201            (	           2606    41268    product mailActor    FK CONSTRAINT     r   ALTER TABLE ONLY eat.product
    ADD CONSTRAINT "mailActor" FOREIGN KEY ("mailActor") REFERENCES eat.actor(mail);
 :   ALTER TABLE ONLY eat.product DROP CONSTRAINT "mailActor";
       eat       postgres    false    197    201    2322            )	           2606    41290    order mailActor    FK CONSTRAINT     r   ALTER TABLE ONLY eat."order"
    ADD CONSTRAINT "mailActor" FOREIGN KEY ("mailActor") REFERENCES eat.actor(mail);
 :   ALTER TABLE ONLY eat."order" DROP CONSTRAINT "mailActor";
       eat       postgres    false    2322    197    205            %	           2606    41273    establishment organizationId    FK CONSTRAINT     �   ALTER TABLE ONLY eat.establishment
    ADD CONSTRAINT "organizationId" FOREIGN KEY ("organizationId") REFERENCES eat.organization(id);
 E   ALTER TABLE ONLY eat.establishment DROP CONSTRAINT "organizationId";
       eat       postgres    false    199    2328    200            �	   �   x���=�0�������n�8�Ȁ�K'�0�5��-�h������y��c/��_�l$fbhQ�G��a��%,Z�%+�c�2ػ�=ek��j�F�J��A��Tn�w���b�4T�'�<�����,�O��*4���d�i��ͷ�Nd���'�XJ�ߋ�E�] ;��hA�/��^���R���V���
��F      �	   )   x�s�t+*�,����9�2�47�+�3 ?��8?�+F��� �
      �	   B   x�KJM,���,J�t+J�KN��K�+I-�41100�4�0�461�����MLN�WH�+��b���� ޽&      �	      x������ � �      �	   A   x��MLN���MLVp��K�I�4�0�A΢�T��Ģ��Դ�RN˭(1/9�������+F��� �h      �	      x������ � �      �	   =   x�s�t��+�9�����%��5O!9��H!%U����ʂĢĒ��<� ΀�ëJR�b���� �FB      �	   %   x�+�-N-�J�t/�/2�8�R�K��=... ��	�      �	   (   x��N�����O/J��M�����,)J�r�t���qqq ��
�     