import { Component, OnInit } from '@angular/core';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(public authService: AuthService) {
    authService.handleAuthentication();
  }

  public ngOnInit(): void {
    if (this.authService.isLoged === true) {
      this.authService.renewTokens();
    }
  }

}
