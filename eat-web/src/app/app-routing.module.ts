import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ConnectComponent } from './pages/connect/connect.component';
import { CallbackComponent } from './pages/callback/callback.component';
import { AuthService } from './shared/services/auth.service';

const routes: Routes = [
  { path: '', component: ConnectComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthService] },
  { path: 'callback', component: CallbackComponent, canActivate: [AuthService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
